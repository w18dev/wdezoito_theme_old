<!DOCTYPE html>
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive-xs.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
</head>
<body>
<header>
	<section id="topo">
		<div class="barraTopo">
			<nav class="navbar navbar-fixed-top menuTopo" style="display: none;">
	    		<div class="container">
					<div id="logoTopo">
					</div>
					<div class="navbar-header ">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="/sobre" id="sobre">SOBRE NÓS</a></li>
							<li><a href="/orcamento" id="seja">ORÇAMENTO</a></li>
							<li><a href="/projetos" id="projetos">PROJETOS</a></li>
							<li><a href="/contato" id="faq">CONTATO</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</nav>
		</div>
		<div class="barraTopoFixo">
			<nav class="navbar ">
	    		<div class="container">
					<div id="logoTopo">
					</div>
					<div class="navbar-header ">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="/sobre" id="sobre">SOBRE NÓS</a></li>
							<li><a href="/orcamento" id="seja">ORÇAMENTO</a></li>
							<li><a href="/projetos" id="projetos">PROJETOS</a></li>
							<li><a href="/contato" id="faq">CONTATO</a></li>
 						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</nav>
		</div>
		<div data-stellar-ratio="2" class="container" data-bottom-top="opacity: 0;" data--33p-top="opacity: 0;" data--66p-top="opacity: 1;">
	    	<div id="textoBanner">
		    	<div id="fundoTextoBanner"></div>
		        <h1>Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui in malesuada enim in.</h1>
		        <h2 class="vamosFechar fadeInLeft"><a href=""><div class="bg-seta1"></div>Vamos fechar?</a></h2>
	        </div>
		</div>

	    <div class="scrollDown">
			<a href=""><div class="bg-seta2 animated infinite bounce"></div></a>
		</div>
	</section>
</header>