<?php
/**
 * @package Wdezoito
 */

get_header(); ?>

<section id="que-fazemos">
	<div class="bgDesign hidden-xs hidden-sm">DESIGN</div>
	<div class="container">
		<div class="col-lg-7">
			<div class="traco"></div>
			<h3> QUER SABER O QUE NÓS FAZEMOS? </h3>
			<div class="textQueFazemos">
				Lorem ipsum dolor sit amet, nec sed ut pede non pretium, ipsum aenean quam id, diam nunc sed sit arcu sodales, Sites Institucionais facilisis a. Dolor adipiscing, in ut, leo E-commerce eros leo sed lacus, dictum sed et, maecenas nulla eu proin tellus. Per est corporis sollicitudin, vel non lectus.
				<br/><br/>Eget integer, et eros venenatis vel augue suscipit. Nunc in suscipit sed. Orci rutrum, libero sed amet hendrerit, mauris a sed maecenas adipiscing, lorem fames senectus metus, adipiscing volutpat vestibulum molestie.
			</div>
			<a class="linkQueFazemos fadeInLeft"><div class="bg-seta3"></div>conheça mais</a>
		</div>
		<div class="col-lg-4 imagemDestaque hidden-xs hidden-sm">

		</div>
	</div>
	<div class="bgDesenvolvimento hidden-xs hidden-sm">DESENVOLVIMENTO</div>
</section>

<section id="nossos-projetos">
	<div class="container">
		<div class="col-lg-8">
			<div class="traco"></div>
			<h3> NOSSOS PROJETOS </h3>
		</div>
		<div class="col-lg-4 btProjetos">
			<div class="btnVoltar"> <div class="bg-seta7" ></div> voltar </div>
			<div class="btnAvancar"> avançar <div class="bg-seta8" ></div> </div>
		</div>
	</div>
	
	<div class="container-fluid col-reset-padding">
		<?php for ($i=1; $i < 9; $i++) : ?> 
			<div class="projeto projetoHover col-lg-3 col-xs-12 col-sm-4 col-reset-padding" rel="<?php echo $i; ?>" style="height:480px; background-color:#000; background-image: url('<?php echo get_template_directory_uri(); ?>/temp/projeto<?php echo $i; ?>.png')">
					<svg class="quadrado" rel="<?php echo $i; ?>" xmlns="http://www.w3.org/2000/svg" width="420"  height="420" viewBox="0 0 420 420"><defs><clipPath id="a"><path fill="#fff" d="M510 2470v-420h420v420z"/></clipPath></defs><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="6" d="M510 2470v-420h420v420z" clip-path="url(&quot;#a&quot;)" transform="translate(-510 -2050)"/></svg>
				<div class="nomeProjeto" rel="<?php echo $i; ?>">
					PROJETO <?php echo $i; ?>
				</div>			
					<div class="linkProjeto" rel="<?php echo $i; ?>" >
					<a class="fadeInLeft"><i class="fa fa-arrow-right" aria-hidden="true"></i>ver projeto</a>
				</div>		
			</div>
		<?php endfor; ?>
	</div>
</section>

<section id="feedbacks">
	<div class="bgPrazo hidden-xs hidden-sm">PRAZO</div>	
	<div class="bgCompromisso hidden-xs hidden-sm">COMPROMISSO</div>	
	<div class="bgQualidade hidden-xs hidden-sm">QUALIDADE</div>
	<div class="container">
		<div class="col-lg-12 col-reset-padding">
			<div class="traco"></div>
			<h3> FEEDBACK DE <br/> ALGUNS CLIENTES </h3>
			<div class="col-lg-9 col-reset-padding textFeedbacks">
				Lorem ipsum dolor sit amet, nec sed ut pede non pretium, ipsum aenean quam id, diam nunc sed sit arcu sodales, facilisis a. Dolor adipiscing, in ut, leo eros leo sed lacus, dictum sed et, maecenas nulla eu proin tellus.
				<br/><br/>Eget integer, et eros venenatis vel augue suscipit. Nunc in suscipit sed. Orci rutrum, libero sed amet hendrerit, mauris a sed maecenas.
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-sm-4 col-xs-12">
				<div class="clienteFeedback">
					Lorem ipsum dolor sit amet, nec sed ut pede non pretium, ipsum aenean quam id”
					<br/><span class="autor"> Roberto Carlos - Diretor / Mampei Funada</span>
				</div>
			</div>
			<div class="col-lg-8 col-sm-8 col-xs-12 col-reset-padding">
				<div class="listFeedbacks">
					<div class="listCarrosel owl-carousel owl-theme">
						<div class="item selected"><img src="<?php echo get_template_directory_uri(); ?>/temp/feedback.png" /></div>
						<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/temp/feedback1.png" /></div>
						<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/temp/feedback2.png" /></div>
						<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/temp/feedback1.png" /></div>
						<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/temp/feedback2.png" /></div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>


<section id="faleconosco">
	<div class="container">
		<div class="col-lg-7 col-sm-8 col-reset-padding">
			<div class="traco"></div>
			<h3> FALE CONOSCO,  <br/>QUEREMOS TE CONHECER </h3>
			<div class="col-lg-11 col-sm-8 col-reset-padding">
				<form>
					<div class="form-group">
						<input type="text" class="form-control" id="nome" placeholder="nome completo*" required>
					</div>
					<div class="form-group col-lg-6 campoColuna col-reset-padding">
						<input type="email" class="form-control" id="email" placeholder="e-mail*" required>
					</div>
					<div class="form-group col-lg-6 campoColuna col-reset-padding">
						<input type="text" class="form-control" id="exampleInputPassword1" placeholder="empresa">
					</div>
					<div class="form-group col-lg-6 campoColuna col-reset-padding">
						<input type="text" class="form-control" id="exampleInputPassword1" placeholder="telefone">
					</div>
					<div class="form-group col-lg-6 campoColuna col-reset-padding">
						<input type="text" class="form-control" id="exampleInputPassword1" placeholder="celular">
					</div>
					<div class="form-group">
						<textarea class="form-control" placeholder="mensagem*" required></textarea>
					</div>
					<button type="submit" class="btn btn-default btnEnviar">enviar</button>
				</form>
				<label class="textOrcamento">Seu site pode ficar do jeito que você imagina!</label>
				<div class="linkOrcamento">
					<a class="fadeInLeft"><div class="bg-seta3"></div> faça um orçamento</a>
				</div>
			</div>
		</div>
		<div class="col-lg-5 col-sm-4 col-reset-padding">
			<div class="img-faleconosco visible-lg visible-sm"></div>
		</div>
	</div>
</section>


<?php get_footer(); ?>