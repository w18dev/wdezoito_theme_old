/*$('.listCarrosel').owlCarousel();*/
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:20,
    nav:true,
    dots: false,
    navText: ["<div class='bg-seta5'></div>","<div class='bg-seta6'></div>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        }
    }
});

jQuery(document).ready(function($) {

    jQuery('.info').css("display", 'none');
    jQuery('.linkProjeto').css("display", 'none');

    jQuery('.projetoHover').mouseenter(function() {
        jQuery('.quadrado[rel="'+$(this).attr('rel')+'"]').css("display", 'block');
        jQuery('.seta[rel="'+$(this).attr('rel')+'"]').css("display", 'block');
        jQuery('.nomeProjeto[rel="'+$(this).attr('rel')+'"]').css("font-weight", '700');
        jQuery('.nomeProjeto[rel="'+$(this).attr('rel')+'"]').css("font-size", '35px');
        jQuery('.linkProjeto[rel="'+$(this).attr('rel')+'"]').fadeIn('slow');
    }).mouseleave(function() {
        jQuery('.quadrado[rel="'+$(this).attr('rel')+'"]').css("display", 'none');
        jQuery('.seta[rel="'+$(this).attr('rel')+'"]').css("display", 'none');
        jQuery('.nomeProjeto[rel="'+$(this).attr('rel')+'"]').css("font-size", '30px');
        jQuery('.linkProjeto[rel="'+$(this).attr('rel')+'"]').fadeOut('slow');
    });


});