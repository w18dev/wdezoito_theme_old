<?php
/**
 *
 * @package Wdezoito
 */
?>




<footer>
	<div class="container divRodape">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<img class="logoFooter" src="<?php echo get_template_directory_uri(); ?>/images/logo-w18-white.png" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 textoRodape">
			Rua Lorem Ipsum Dolor - Jd. Sit Amet<br/>
			Presidente Prudente - SP<br/><br/>

			contato@wdezoito.com.br<br/>
			(18)3355 2020
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 footer-direitos">
			TODOS OS DIREITOS RESERVADOS - WDEZOITO 2017
		</div>
	</div>
</footer>


<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/parallax.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>


</body>
</html>